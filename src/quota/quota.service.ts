import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Quota } from './schema/quota.schema';

@Injectable()
export class QuotaService {
  constructor(
    @InjectModel(Quota.name)
    private readonly quotaModel: Model<Quota>,
  ) {}

  async findByUserId(userId: string): Promise<Quota | null> {
    const quota = await this.quotaModel.findOne({ userId: userId }).exec();
    return quota;
  }
  async create(userId: string): Promise<Quota> {
    const existingQuota = await this.quotaModel.findOne({ userId: userId });
    if (existingQuota) {
      throw new BadRequestException(`Quota already exists for user `);
    }
    // Create a new document
    const createdQuota = new this.quotaModel({ userId: userId, number: 10 });
    return createdQuota.save();
  }

  async update(userId: string): Promise<Quota> {
    const existingQuota = await this.quotaModel
      .findOne({ userId: userId })
      .exec();

    if (!existingQuota) {
      throw new NotFoundException('Quota not found for this user. ');
    }

    // Decrement the 'nombre' property
    if (existingQuota.number > 0) {
      existingQuota.number -= 1;
    } else {
      // Optionally handle the case where 'nombre' is already zero
      throw new BadRequestException("Quota already reached it's limit");
    }

    // Save the updated document
    const updatedQuota = await existingQuota.save();

    return updatedQuota;
  }
}
