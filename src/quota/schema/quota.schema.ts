import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

@Schema()
export class Quota extends Document {
  @Prop({ required: true })
  @ApiProperty()
  userId: string;

  @Prop({ required: true })
  @ApiProperty()
  number: number;
}

export const QuotaSchema = SchemaFactory.createForClass(Quota);
