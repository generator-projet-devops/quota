import { Controller, Post, Put, Request, UseGuards } from '@nestjs/common';
import { QuotaService } from './quota.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Quota } from './schema/quota.schema';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

@ApiTags('Quota')
@Controller('quota')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class QuotaController {
  constructor(private readonly quotaService: QuotaService) {}

  @Post()
  @ApiOperation({ summary: 'Create a Quota' })
  @ApiResponse({
    status: 201,
    description: 'Quota created successfully',
    type: Quota,
  })
  create(@Request() req) {
    return this.quotaService.create(req.user.id);
  }

  @Put()
  @ApiOperation({ summary: 'Update a Quota ' })
  @ApiResponse({
    status: 200,
    description: 'Quota updated successfully',
    type: Quota,
  })
  update(@Request() req) {
    return this.quotaService.update(req.user.id);
  }
}
