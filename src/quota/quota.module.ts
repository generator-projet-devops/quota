import { Module } from '@nestjs/common';
import { QuotaService } from './quota.service';
import { QuotaController } from './quota.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { QuotaSchema } from './schema/quota.schema';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt-strategy';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Quota', schema: QuotaSchema }]),
  ],
  controllers: [QuotaController],
  providers: [QuotaService, LocalStrategy, JwtStrategy, JwtService],
})
export class QuotaModule {}
