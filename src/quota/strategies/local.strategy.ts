import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { QuotaService } from '../quota.service';
import { Quota } from '../schema/quota.schema';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private quotaService: QuotaService) {
    super({
      usernameField: 'userId',
    });
  }

  async validate(userdId: string): Promise<Quota> {
    // Check if the user exists in the database
    let quota = await this.quotaService.findByUserId(userdId);
    // If the user doesn't exist, create a new one
    if (!quota) {
      quota = await this.quotaService.create(userdId);
    }

    return quota;
  }
}
